﻿using homework_with_parsing_urls.src;
using homework_with_parsing_urls.src.Services;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace homework_with_parsing_urls
{
    class Program
    {
        static void Main(string[] args)
        {
            DataLoader dataLoader = new DataLoader();
            string html = dataLoader.GetDataFromUrl("https://regex101.com/");

            TextParser textParser = new TextParser();
            MatchCollection matches = textParser.GetUrlsFromText(html);

            Console.WriteLine("Найдено совпадений: " + matches.Count);

            foreach (Match match in matches)
            {
                Console.WriteLine(match.Groups["url"].Value);
            }
        }
    }
}
