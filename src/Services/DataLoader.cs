﻿using homework_with_parsing_urls.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace homework_with_parsing_urls.src.Services
{
    class DataLoader : IDataLoader
    {
        public string GetDataFromUrl(string url)
        {
            var webClient = new WebClient();

            var result = webClient.DownloadString(url);
            webClient.Dispose();

            return result;
        }
    }
}
