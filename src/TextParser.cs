﻿using homework_with_parsing_urls.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace homework_with_parsing_urls.src
{
    class TextParser : ITextParser
    {
        public MatchCollection GetUrlsFromText(string text)
        {
            const string pattern = @"<[^>]*?(src|href)[\s]*?=[\s]*[\""\']?(?<url>[^\""\""\']+)[\""\']?[^>]*?\/?>";
            const RegexOptions options = RegexOptions.IgnoreCase;
            
            MatchCollection matches = Regex.Matches(text, pattern, options);
            
            return matches;            
        }
    }
}
