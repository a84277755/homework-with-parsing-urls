﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework_with_parsing_urls.src.Interfaces
{
    interface IDataLoader
    {
        public string GetDataFromUrl(string url);
    }
}
