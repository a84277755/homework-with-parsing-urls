﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace homework_with_parsing_urls.src.Interfaces
{
    interface ITextParser
    {
        public MatchCollection GetUrlsFromText(string text);
    }
}
